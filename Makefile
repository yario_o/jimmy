CC = gcc
SRCS = jimmy.c linux.c
OBJS = $(SRCS:.c=.o)
CFLAGS = -Wall -pedantic -fPIC -ffunction-sections -fdata-sections
PROG = libjimmy.a

all: $(PROG)

$(OBJS): jvm.h mem.h

$(PROG): $(OBJS)
	gcc-ar rcs $(PROG) $(OBJS)

clean:
	rm $(OBJS) $(PROG)
