# Java Interface via Memory Made by Yario (jimmy)
jimmy is a library to interact with a jvm process only by reading and writing to its memory.

[Download](https://gitlab.com/yario_o/jimmy/-/releases)

## License
[MIT](https://choosealicense.com/licenses/mit/)
