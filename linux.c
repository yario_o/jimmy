#include "mem.h"
#define _GNU_SOURCE
#define __USE_GNU
#include <sys/uio.h>
#include <unistd.h>
#include <string.h>
#include "jimmy.h"

static pid_t pid;

void readmem(void *buf, const void *addr, size_t size) {
	struct iovec local = { buf, size };
	struct iovec remote = { (void *) addr, size };
	if (process_vm_readv(pid, &local, 1, &remote, 1, 0) != size)
		memset(buf, 0, size);
}
void writemem(void *buf, void *addr, size_t size) {
	struct iovec local = { buf, size };
	struct iovec remote = { (void *) addr, size };
	if (process_vm_writev(pid, &local, 1, &remote, 1, 0) != size)
		memset(buf, 0, size);
}

void jmy_init(pid_t jvmid, const void *jvmbase) {
	pid = jvmid;
	jvm = jvmbase;
}
